---
title: "SLR sandbox"
author: "Alexis"
date: "December 1, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2);
library(treemapify);
library(stringr);
library(xtable);
library(binom);

pdftee <- function(filename, p, hcm = 10.76/2, wcm = 10.76) {
  pdf(filename, height = hcm/2.54, width = wcm/2.54);
  print(p);
  dev.off();
  print(p);
}
fixCase <- function(string) {
  if (length(grep("[a-z]", string))) {
    string <- str_to_title(string);
  }
  return (string);
}
theme_set(theme_bw(base_family = "Times", base_size = 9));
```

## R Markdown

Load csv data and create inc.csv.
```{r r001}
m.df <- read.csv("../manual.csv");
inc.df <- subset(m.df, manual.included == 1 & is.na(cnt.excluded))
write.csv(inc.df, file = "inc.csv");
```

Histogram of publication years:
```{r r002}
pdftee("../../../plots/slr-hist-years.pdf", 
       ggplot(inc.df, aes(x=year)) + labs(x="Year", y="Studies")  + geom_bar(), hcm = 3.555, wcm = 7.11);
```

Treemap of categories
```{r r003}
cleanupCategory <- function(string) {
  string <- trimws(string);
  string <- gsub("\\(([A-Za-z]*)\\)", "@\\1@", string);
  string <- gsub(" ", "", string);
  string <- gsub("_", " ", string);
  string <- gsub("process language", "workflow language", string);
  string <- gsub("\\([^)]*\\)", "", string);
  string <- gsub("@([^@]*)@", "(\\1)", string);
  string <- gsub("([^/]*)/([^(]*)\\(([^)]*)\\)", "\\1/\\2", string);
  string <- gsub("proxy\\(([^)]*)\\)", "proxy/\\1", string);
  #string <- ifelse(grepl("proxy", string), gsub("\\(([^)]*)\\)", "/\\1", string), string);
  return(string);
};
protpars <- function(string) {
  string <- gsub("\\(", "\\\\(", string);
  string <- gsub("\\)", "\\\\)", string);
  return(string);
}
clean.cats <- cleanupCategory(inc.df$category);
un.cats <- unique(unlist(strsplit(as.character(clean.cats), ";")))

counts <- c();
childs <- c();
parents <- c();
for(cat in un.cats) {
  counts <- c(counts, length(grep(paste(protpars(cat),"(;|$)",sep=""), clean.cats)));
  parent <- gsub("^([^/]*).*", "\\1", cat);
  parent <- paste(parent," (",length(grep(paste("(^|;)",parent,sep=""), clean.cats)),")",sep="")
  parents <- c(parents, fixCase(parent));
  childs <- c(childs, fixCase(gsub("^([^/]*/)?([^/]*)$", "\\2", cat)));
}
tm.df <- data.frame(parent = parents, child = childs, count = counts);

pdftee("../../../plots/slr-categories.pdf", 
       ggplot(tm.df, aes(area = count, fill = parent, subgroup = parent, 
                         label = paste(child," (",count,")",sep=""))) + 
         geom_treemap() + geom_treemap_subgroup_border() + 
         geom_treemap_subgroup_text(family = "sans", size=13) +
         geom_treemap_text(family = "Times", reflow=T, size=10, min.size=2) +
         theme(legend.position = "bottom", legend.margin = margin(), 
               legend.box.margin = margin(t=-8), legend.key.height = unit(9, "pt") ) +
         labs(fill = "Category"),
       hcm = 10.76*0.66, wcm = 10.76);

ord.df <- tm.df[order(tm.df$parent, -tm.df$count),];
ord.df$parent <- gsub("([^(]*) \\(.*\\)", "\\1", ord.df$parent);
print(xtable(ord.df), include.rownames = F);
```
```{r r004}
tl.cats <- unique(gsub("^([^/]*).*$", "\\1", un.cats));
supp <- str_to_lower(inc.df$supp);

cats.supp <- function(tl.cats, clean.cats) {
  cats <- c();
  groups <- c();
  counts <- c();
  for (cat in tl.cats) {
    cats <- c(cats, fixCase(cat));
    groups <- c(groups, "SOAP+REST");
    counts <- c(counts, length(intersect(grep(paste("(^|;)",cat,sep=""), clean.cats), 
                                  setdiff(intersect(grep("soap", supp), grep("rest", supp)),
                                          grep("eos", supp)))));
    cats <- c(cats, fixCase(cat));
    groups <- c(groups, "SOAP+EOS");
    counts <- c(counts, length(intersect(grep(paste("(^|;)",cat,sep=""), clean.cats), 
                                  setdiff(intersect(grep("soap", supp), grep("eos", supp)),
                                          grep("rest", supp)))));
    cats <- c(cats, fixCase(cat));
    groups <- c(groups, "REST+EOS");
    counts <- c(counts, length(intersect(grep(paste("(^|;)",cat,sep=""), clean.cats), 
                                  setdiff(intersect(grep("rest", supp), grep("eos", supp)),
                                          grep("soap", supp)))));
    cats <- c(cats, fixCase(cat));
    groups <- c(groups, "SOAP+REST+EOS");
    counts <- c(counts, length(intersect(grep(paste("(^|;)",cat,sep=""), clean.cats), 
                                  intersect(intersect(grep("soap", supp), grep("rest", supp)), grep("eos", supp)))));
  }
  return(data.frame(cat = cats, group = groups, count = counts));
}
cats.supp.ggplot <- function(tm.df) {
  return(ggplot(tm.df, aes(area = count, fill = cat, subgroup = group, 
                         label = paste(cat," (",count,")",sep=""))) + 
         geom_treemap() + geom_treemap_subgroup_border() + 
         geom_treemap_subgroup_text(family = "sans", size=9, min.size=2) +
         geom_treemap_text(family = "Times", reflow=T, size=9, min.size=2) +
         theme(legend.position = "bottom", legend.margin = margin(), 
               legend.box.margin = margin(t=-8), legend.key.height = unit(9, "pt") ) +
         labs(fill = "Category"));
}

tm.df <- cats.supp(tl.cats, clean.cats);
pdftee("../../../plots/slr-cat+type.pdf", 
       cats.supp.ggplot(tm.df),
       hcm = 10.76*0.66, wcm = 10.76);

ord.df <- tm.df[order(tm.df$group, -tm.df$cat),];
ord.df <- ord.df[c("group", "cat", "count")];
colnames(ord.df) <- c("Service Types", "Method", "Count");
print(xtable(ord.df), include.rownames = F);
```

```{r r005}

length(grep("workflow language", clean.cats)); # 26
length(grep("middleware", clean.cats));       # 19
length(intersect(grep("workflow language", clean.cats), grep("middleware", clean.cats)))             # 12
length(intersect(grep("event processor", clean.cats), grep("middleware", clean.cats)))              # 2
length(intersect(intersect(grep("workflow language", clean.cats, invert = T), grep("event processor", clean.cats, invert = T)), grep("middleware", clean.cats))) # 6
length(intersect(grep("workflow language", clean.cats, invert = T), grep("middleware", clean.cats))) # 7
length(intersect(grep("workflow language", clean.cats), grep("middleware", clean.cats, invert = T))) # 14

inc.df[intersect(intersect(grep("workflow language", clean.cats, invert = T), grep("event processor", clean.cats, invert = T)), grep("middleware", clean.cats)),]

```
```{r r006}
tl.cats <- unique(gsub("^([^/]*).*$", "\\1", un.cats));

cats.rel <- function(clean.cats) {
  outers <- c();
  inners <- c();
  values <- c();
  counts <- c();
  for (outer in tl.cats) {
    my.inners <- c();
    my.counts <- c();
    for (inner in setdiff(tl.cats, c(outer))) {
      count <- length(intersect(grep(paste("(^|;)",outer,sep=""), clean.cats), grep(paste("(^|;)",inner,sep=""), clean.cats)));
      if (count > 0) {
        my.inners <- c(my.inners, fixCase(inner));#paste(inner,"(",count,")",sep=""));
        my.counts <- c(my.counts, count);
      }
    }
    count <- length(grep(paste("^(",outer,"[^;]*;?)*$",sep=""), clean.cats));
    if (count > 0) {
      my.inners <- c(my.inners, "[none]");
      my.counts <- c(my.counts, count);
    }
    outers <- c(outers, rep(paste(fixCase(outer)," (",length(grep(paste("(^|;)",outer,sep=""),clean.cats)),")",sep=""), 
                            length(my.inners)));
    inners <- c(inners, my.inners);
    counts <- c(counts, my.counts);
    values <- c(values, (my.counts/sum(my.counts))*length(grep(outer,clean.cats)));
  }
  clean.cats[grep("(^|;)proxy", clean.cats)]
  
  return(data.frame(outer = outers, inner = inners, value = values, count = counts));
}
cats.rel.ggplot <- function(tm.df) {
  return (ggplot(tm.df, aes(area = value, fill = inner, subgroup = outer, 
                         label = paste(inner," (",count,")",sep=""))) + 
         geom_treemap() + geom_treemap_subgroup_border() + 
         geom_treemap_subgroup_text(family = "sans", size=9, min.size=2) +
         geom_treemap_text(family = "Times", reflow=T, size=9, min.size=2) +
         theme(legend.position = "bottom", legend.margin = margin(), 
               legend.box.margin = margin(t=-8), legend.key.height = unit(9, "pt") ) +
         labs(fill = "Category"));
}
tm.df <- cats.rel(clean.cats);
pdftee("../../../plots/slr-cat+cat.pdf", 
       cats.rel.ggplot(tm.df),
       hcm = 10.76*0.66, wcm = 10.76);
```

```{r r007}
roi.2 <- function(a, b, forbid) {
  return(setdiff(intersect(grep(a, inc.df$supp), grep(b, inc.df$supp)), grep(forbid, inc.df$supp)));
}
roi.3 <- function(a, b, c) {
  return(intersect(intersect(grep("soap", inc.df$supp), grep("eos", inc.df$supp)),
                                              grep("rest", inc.df$supp)));
}

cats.rel.ggplot(cats.rel(clean.cats[    roi.2("rest", "soap", "eos")])) + 
  labs(title = paste("REST+SOAP",length(roi.2("rest", "soap", "eos"))));
cats.rel.ggplot(cats.rel(clean.cats[   roi.2("rest", "eos", "soap")])) + 
  labs(title = paste("REST+EOS",length(roi.2("rest", "eos", "soap"))));
cats.rel.ggplot(cats.rel(clean.cats[   roi.2("soap", "eos", "rest")])) + 
  labs(title = paste("SOAP+EOS",length(roi.2("soap", "eos", "rest"))));
cats.rel.ggplot(cats.rel(clean.cats[        roi.3("rest", "soap", "eos")])) + 
  labs(title = paste("REST+SOAP+EOS",length(roi.3("rest", "soap", "eos"))));

```
```{r r008}
cleanupEval <- function(string) {
  string <- trimws(string);
  string <- gsub(" ", "", string);
  string <- gsub("_", " ", string);
  string <- gsub("\\([^)]*\\)", "", string);
  return(string);
};
clean.evals <- cleanupEval(inc.df$heter.eval);
un.evals <- unique(unlist(strsplit(as.character(clean.evals), ";")));
clean.evalqs <- cleanupEval(inc.df$heter.eval.quality);
un.evalqs <- setdiff(unique(clean.evalqs), c(""));

tm.df <- cats.supp(un.evals, clean.evals);
pdftee("../../../plots/slr-eval+supp.pdf", 
       cats.supp.ggplot(tm.df),
       hcm = 10.76*0.66, wcm = 10.76);

ord.df <- tm.df[order(tm.df$group, -tm.df$cat),];
ord.df <- ord.df[c("group", "cat", "count")];
colnames(ord.df) <- c("Service Types", "Evaluation Method", "Count");
print(xtable(ord.df), include.rownames = F);

groups <- c();
cats <- c();
counts <- c();
for (group in un.evals) {
  for (cat in un.evalqs) {
    rx <- gsub("\\(", "\\\\(", group);
    count <- length(intersect(grep(rx,clean.evals, fixed = T), which(clean.evalqs == cat)));
    if (count > 0) {
      groups <- c(groups, fixCase(group));
      cats <- c(cats, fixCase(cat));
      counts <- c(counts, count);
    }
  }
}
tm.df <- data.frame(group = groups, cat = cats, count = counts);
pdftee("../../../plots/slr-eval+qlty.pdf", 
       ggplot(tm.df, aes(area = count, fill = cat, subgroup = group, 
                         label = paste(cat," (",count,")",sep=""))) + 
         geom_treemap() + geom_treemap_subgroup_border() + 
         geom_treemap_subgroup_text(family = "sans", size=9, min.size=2) +
         geom_treemap_text(family = "Times", reflow=T, size=9, min.size=2) +
         theme(legend.position = "bottom", legend.margin = margin(), 
               legend.box.margin = margin(t=-8), legend.key.height = unit(9, "pt") ) +
         labs(fill = "Quality"),
       hcm = 10.76*0.66, wcm = 10.76);

ord.df <- tm.df[order(tm.df$group, -tm.df$cat),];
ord.df <- ord.df[c("group", "cat", "count")];
colnames(ord.df) <- c("Evalution Method", "Evaluation Quality", "Count");
print(xtable(ord.df), include.rownames = F);
```

## CIs for false exclusion/inclusion
```{r load.cis}
reeval <- read.csv("../reeval.csv");
prop.test(length(which(reeval$fe == 1)), nrow(reeval))$conf.int
binom.confint(length(which(reeval$fe == 1)), nrow(reeval), methods = c("exact"))
x <- binom.confint(length(which(reeval$fi == 1)), nrow(reeval), methods = c("exact"))
c(x$mean*100, x$lower*100, x$upper*100, x$lower*386, x$upper*386)
x <- binom.confint(length(which(reeval$definitive_fe == 1)), nrow(reeval), methods = c("exact"))
c(x$mean*100, x$lower*100, x$upper*100, x$lower*386, x$upper*386)

binom.confint(1, 97, methods = c("exact"))
binom.confint(0, 5, methods = c("exact"))

x <- binom.bayes(length(which(reeval$definitive_fe == 1)), nrow(reeval), prior.shape1=.5, prior.shape2 = 1)
c(x$mean*100, x$lower*100, x$upper*100, x$lower*386, x$upper*386)
x <- binom.bayes(length(which(reeval$definitive_fe == 1)), nrow(reeval))
c(x$mean*100, x$lower*100, x$upper*100, x$lower*386, x$upper*386)

binom.bayes.densityplot(x) + scale_x_continuous(limits = c(0, 0.2))

```