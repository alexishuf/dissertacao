### Seções prontas
* abstract/resumo/resumo estendido (main.tex)
* agradecimntos
* 1, 3, 5 e 6

### professores prontos:
    (sem considerar anotações do PDF)
* vítor
* gustavo
* elder
* (quase) willrich

### Comentários
Legenda: 
- pendente
@ feito
+ não vou fazer

vitor:
@ hateoas, exemplos de controles [ %BANCA-6 ]
  expliquei com exemplo, 
@ web apis  == rest degenarado [ %BANCA-5 ]
  bg de interactionmodels e na avaliação de heterogeneidades
@ adjetivo ou substantivo da arquitetura
@ letra maiuscula unserved
@ agregação trocada em Composite_Service
@ composto vs. atomico conjuntos de generalizaçãoes [ %BANCA-2 ]
@ descrever o processo de criação da ontologia [ %BANCA-3 ]
@ figura da ontologia [ %BANCA-1 ]
  Mudei pra seguir a UML quase que totalmente, listei as minhas 
  mudanças no texto: "," e "* -> 1" por default
@ core ontology? [ %BANCA-1 ]
@ apendice módulos unsered-x [ %BANCA-1 ]
  o apendice ficou bem seco, vale uma revisada rápida. Mas deixo 
  avisado que se for pra fazer uma descrição fluída, vai ficar grande.
@ SPARQL e SPIN no capítulo 2 [ %BANCA-4 ]
+ conceitos do capitulo 4 sem correspondente no cap 2 [ %BANCA-7 ]
  com preguiça de encontrar novos, mas melhorei a descrição do 
  Hydra no bg
@ explicar melhor os termos lifted e lowered
@ modelo de protocolo: D = descrição
  repetido na legenda, tava no final da seção
+ RSL sem ACM
  tá explicado no apendice

vitor.pdf:
+ "sugiro" publicações na introdução
  Pra não duplicar seção, casualmente citei eles no 1.4 "Document structure"
@ alguns data elements não estão na figura da arquitetura
  Resolvido, só não consegui colocar "service data"



gustavo:
@ atualizar a hipótese
  não tava tão grave quanto achei quando ele falou na apresentação. 
  Mantive a hipótese no body.tex inalterada e alterei a do resumo 
  estendido pra ficar mais alinhada com a principal (foi no resumo 
  estendido que ele anotou)
@ contribuições no objetivo? seprar em objetivos especificos
  No resumo extendido e na introdução coloquei contribuições em 
  seção separada. Os itens do objetivo que estavam na apresentação 
  também foram pra dissertação mas não usei a palavra "objetivo 
  específico". Esses itens são um detalhamento do objetivo e embora 
  eu já tenha visto e lido objetivos específicos que são um 
  detalhamento (sem serem contribuções ou entregáveis) eu não tenho 
  certeza se isso é certo. Do jeito que deixei ninguém deve achar 
  problema.
@ expressividade não é experimental [ %BANCA-9 ]
  o gustavo reclamou no resumo estendido. Adicionei 1 pgf lá e no 
  texto principal só uma frase.
@ replicação não é tolerafncia a falta
@ métricas do problema são mais pesadas que métricas da solução
  Em cima da tab:wsc-problem, pgf anterior: "In general, ..."
@ replicações problemáticas se muitas notificações [ %BANCA-8 ] a
  frase onde ele fez essa anotação sumiu do resumo est. e das
  conclusões com minha reescrita durante as férias. Então adicionei
  essa discussão quando falo do código. Achei muito específico pra
  colocar nas limitações
@ explicar minimize [ %BANCA-10 ]
  expliquei melhor e separei em um parágrafo dedicado ao assunto, deve
  resolver a confusão com o COMPAT
@ explicar COMPAT
  tinha frase explicando, melhorei um pouco. Mas acho que ele se
  confudiu pelo minimize
@ gráficos sem unidade
@ Explicar que o cenário não existe fisicamente [ %BANCA-11 ]
  Resolvido com footnote na primeira frase. Aproveitei e melhorei a
  descrição do cenário colocando a figura dos slides. Pra explicar a
  execução mantive a original. Não troquei pelo diagrama de sequência,
  como o márcio sugeriu pois nenhum membro da banca reclamou disso.



elder:
@ reforçar por que ontologia
  Resolvido no BANCA-1
@ falar das regras customizadas do jena [ %BANCA-12 ]
@ falar do reasoner especializado na seleção de componentes [ %BANCA-12 ]
@ mundo aberto mundo fechado  [ %BANCA-12 ]
  Resolvi os 3 acima com "4.1.2.1 Ontology reasoning"
@ conclusões: motor de inferência, triple store, estender o cenário em
  um ambiente de produção. [ %BANCA-13 ]
  Transformei a frase em um pgf
@ multi-contexto em trabalhos futuros
@ Se não conseguir montar a composição? [ %BANCA-17 ]
  2 frases, no começo e no final da seção 4.2
@ trabalho futuro: relaxar objetivos do cliente quando eles não podem
  ser atingidos [ %BANCA-14 ]
@ 5.1 ligar expressividade com suporte a heterogeneidade [ %BANCA-15 ]
@ Expressividade: suporta negação via OWL [ %BANCA-15 ]
@ dizer por que negação e loops não são suportadas no unserved [ %BANCA-15 ]
  resolvi esses três pontos com uma mechida pesada no 5.1.2. Mudei
  mais no começo e no fim.


elder.docx: 
@ unserved architecture 
  "The unserved architecture" substituído por "Unserved" em vários
  pontos. O U maíusculo permitiu remover o architecture em vários
  lugares. O nome foi explicado no começo do capítulo 4, em footnote
  pra não atrapalhar a fluidez.
@ detalhes do texto
  O elder escreve um resumo daonde eu tirei algumas idéias pra
  melhorar o meu. Mas muita coisa não incorporei pois pioraria os
  problemas que o willrich apontou, de conceitos não definidos.



willrich:
- mais exemplos: ontologia usar exemplo pra explicar
- usar exemplo pra explicar a proposta
@ modelo de interação no resumo estendido
@ controle hipermídia no res extendido
@ resumo estendido conceitos
  o que dava de resolver com poucas palavras, resolvi. Tem coisas que
  só posso deixar o nome e citação.
@ tirar marginnotes
@ comparar de melhor pra pior ou comparar métodos [ %BANCA-16 ]
@ incorporar evaluation dos trabalhos (slr-eval) no texto
+ Anexo C dos trabalhos relacionados: versão abreviada deve ir pro
  corpo do trabalho.
  Abreviar vai (1) demorar, (2) remover várias coisas que respondi
  usando os anexos, (3) deixar a seção 3.1 grande demais.
@ começo da proposta clarificar tipo de heterogeneidade
  troquei no começo e deixei bem explicado que é heterogeneidade entre
  modelos de interação e mencionei HATEOAS e EOS. Passei por usos de
  "heterogeneity" e coloquei \interactionmodel onde cabia pra deixar
  isso claro.
- adiantar exemplo da arquitetura pra explicar ontologia
@ precisa de ontologia? 
  Resolvido no BANCA-1
@ termo replicação => fork
- puxar algo da avaliação de heterogeneidade pra proposta?
@ critérios para avaliação da proposta
  No método e no resumo estendido o único cirtério claro é tempo. Mudei a seção "Method" pra deixar ainda mais claro que o objetivo é "menor ou igual". Pros outros tipos de avaliação fica quase impossível explicar no resumo ou na introdução. Pois ou cito coisas sem definição (e sem base alguma naquele ponto) ou teria que puxar umas 2-3 páginas resumindo tipos de serviço, modelos de interação e descrição de serviço. Já no caso do cenário não tem critério prévio 
@ Retirar marcações de definições no PDF.

willrich.pdf:
@ Muito detalhe sobre connecting elements antes da 4.1.1
  Resolvi cortando o que tava redundante com trabalhos futuros.
