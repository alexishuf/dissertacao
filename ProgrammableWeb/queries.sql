DROP TABLE IF EXISTS tmp; 
CREATE TABLE tmp (url TEXT, 
                  isREST INTEGER, 
				  isHypermedia INTEGER, 
                  isSOAP INTEGER, 
                  usesXML INTEGER,
                  isWebApi INTEGER,
                  noData INTEGER, 
                  PRIMARY KEY(url));  

INSERT INTO tmp(url, isREST, isSOAP, usesXML, isWebApi, noData) SELECT url, 0, 0, 0, 0, 0 FROM apis;

UPDATE tmp SET isREST = 1 WHERE url IN (
	SELECT url FROM apis WHERE UPPER(Protocol_Formats) LIKE '%REST%' OR 
	                           UPPER(Other_options) LIKE '%REST%' OR 
	                           UPPER(Supported_Request_Formats) LIKE '%REST%' OR 
	                           UPPER(Supported_Response_Formats) LIKE '%REST%' OR 
	                           UPPER(Architectural_Style) LIKE '%REST%' OR 
	                           UPPER(Type) LIKE '%REST%' OR 
	                           UPPER(Hypermedia_API) LIKE '%YES%'
);

UPDATE tmp SET isHypermedia = 1 WHERE url IN (
	SELECT url FROM apis WHERE UPPER(Hypermedia_API) LIKE '%YES%'
);

UPDATE tmp SET isSOAP = 1 WHERE url IN (
	SELECT url FROM apis WHERE UPPER(Protocol_Formats) LIKE '%SOAP%' OR 
	                           UPPER(Other_options) LIKE '%SOAP%' OR 
	                           UPPER(Supported_Request_Formats) LIKE '%SOAP%' OR 
	                           UPPER(Supported_Response_Formats) LIKE '%SOAP%' OR 
	                           UPPER(Architectural_Style) LIKE '%SOAP%' OR 
	                           UPPER(Type) LIKE '%SOAP%'
);

UPDATE tmp SET usesXML = 1 WHERE url IN (
	SELECT url FROM apis WHERE UPPER(Protocol_Formats) LIKE '%XML%' OR 
	                           UPPER(Supported_Request_Formats) LIKE '%XML%' OR 
	                           UPPER(Supported_Response_Formats) LIKE '%XML%'
);

UPDATE tmp SET isWebApi = 1 WHERE url IN (
    SELECT tmp.url FROM tmp, apis WHERE tmp.url=apis.url AND (
                                  isREST = 1 OR  (
                                      isSOAP = 0 AND (
                                          apis.Protocol_Formats NOT NULL OR
                                          apis.Supported_Request_Formats NOT NULL OR
                                          apis.Supported_Response_Formats NOT NULL
                                      )
                                  )
                               )
);

UPDATE tmp SET noData = 1 WHERE url IN (
	SELECT url FROM apis WHERE UPPER(Protocol_Formats) IS NULL AND 
	                           UPPER(Other_options) IS NULL AND
	                           UPPER(Supported_Request_Formats) IS NULL AND
	                           UPPER(Supported_Response_Formats) IS NULL AND 
	                           UPPER(Architectural_Style) IS NULL AND
	                           UPPER(Type) IS NULL
);

DROP TABLE IF EXISTS results; 
CREATE TABLE results (filter TEXT,  percent REAL);  
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=1', count(url)/16473.0*100 FROM tmp WHERE isSOAP=1;
INSERT INTO results (filter, percent) 
	SELECT 'isREST=1', count(url)/16473.0*100 FROM tmp WHERE isREST=1;
INSERT INTO results (filter, percent) 
	SELECT 'isHypermedia=1', count(url)/16473.0*100 FROM tmp WHERE isHypermedia=1;
INSERT INTO results (filter, percent) 
	SELECT 'isWebApi=1', count(url)/16473.0*100 FROM tmp WHERE isWebApi=1;
INSERT INTO results (filter, percent) 
	SELECT 'isWebApi=1 AND isREST=0', count(url)/16473.0*100 FROM tmp WHERE isWebApi=1 AND isREST=0;
INSERT INTO results (filter, percent) 
	SELECT 'noData=1', count(url)/16473.0*100 FROM tmp WHERE noData=1;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=0 AND isWebApi=0', count(url)/16473.0*100 FROM tmp WHERE isSOAP=0 AND isWebApi=0;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=1 AND isWebApi=0', count(url)/16473.0*100 FROM tmp WHERE isSOAP=1 AND isWebApi=0;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=1 AND isWebApi=1', count(url)/16473.0*100 FROM tmp WHERE isSOAP=1 AND isWebApi=1;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=1 AND isREST=1', count(url)/16473.0*100 FROM tmp WHERE isSOAP=1 AND isREST=1;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=1 AND isWebApi=1 AND isREST=0', count(url)/16473.0*100 FROM tmp WHERE isSOAP=1 AND isWebApi=1 AND isREST=0;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=1 AND isHypermedia=1', count(url)/16473.0*100 FROM tmp WHERE isSOAP=1 AND isHypermedia=1;
INSERT INTO results (filter, percent) 
	SELECT 'isSOAP=0 AND usesXML=1 AND isREST=0', count(url)/16473.0*100 FROM tmp WHERE isSOAP=0 AND usesXML=1 AND isREST=0;

