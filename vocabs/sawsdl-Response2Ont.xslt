<?xml version='1.0' ?>
<xsl:transform version="2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:po="http://www.w3.org/2002/ws/sawsdl/spec/wsdl/order#"
  xmlns:POOntology="http://www.w3.org/2002/ws/sawsdl/spec/ontology/purchaseorder#">
  <xsl:output method="xml" version="1.0" encoding="iso-8859-1" indent="yes" />
  <xsl:template match="/">
    <rdf:RDF>
      <POOntology:OrderConfirmation>
        <hasStatus rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
          <xsl:value-of select="po:OrderResponse" />
        </hasStatus>
      </POOntology:OrderConfirmation>
    </rdf:RDF>  
  </xsl:template>
</xsl:transform>