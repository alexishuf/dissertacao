package org;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.jbibtex.*;

import java.io.*;
import java.util.*;

/**
 * Hello world!
 *
 */
public class App  {
    public static void main( String[] args ) throws Exception {
        File dir = new File("/home/alexis/ufsc/dissertacao/revisao_avaliacao/v1");

        if (!new File(dir, "all.csv").exists()) {
            updateAll(Collections.emptyList(), dir);
        } else {
            try (FileReader csvFileReader = new FileReader(new File(dir, "all.csv"));
                 CSVParser all = new CSVParser(csvFileReader, CSVFormat.DEFAULT.withHeader())) {
                List<CSVRecord> records = all.getRecords();
                updateAll(records, dir);
            }
        }

        try (FileReader csvFileReader = new FileReader(new File(dir, "all.csv"));
             CSVParser all = new CSVParser(csvFileReader, CSVFormat.DEFAULT.withHeader())) {
            List<CSVRecord> records = all.getRecords();
            generateAbstracts(records, dir);
        }
    }

    private static void generateAbstracts(List<CSVRecord> all, File dir) throws Exception {
        File[] files = dir.listFiles(name -> name.getName().endsWith(".bib"));
        if (files == null) throw new IllegalArgumentException("No .bib files");

        BibTeXParser parser = new BibTeXParser();
        try (FileWriter csvFileWriter = new FileWriter(new File(dir, "abstracts.csv"));
             CSVPrinter csv = new CSVPrinter(csvFileWriter, CSVFormat.DEFAULT)) {
            csv.printRecord("id", "title", "abstract");

            for (File bibFile : files) {
                try (FileReader bibFileReader = new FileReader(bibFile)) {
                    BibTeXDatabase db = parser.parseFully(bibFileReader);
                    for (BibTeXEntry e : db.getEntries().values()) {
                        Value doi = e.getField(BibTeXEntry.KEY_DOI);
                        String doiString = doi == null ? null : doi.toUserString();
                        String title = e.getField(BibTeXEntry.KEY_TITLE).toUserString();
                        String author = e.getField(BibTeXEntry.KEY_AUTHOR).toUserString();
                        Value anAbstract = e.getField(new Key("abstract"));
                        String abstractString = anAbstract == null ? null
                                : anAbstract.toUserString();

                        CSVRecord record = find(all, doiString, title, author);
                        if (record == null) {
                            System.err.printf("Found no record for %s, %s, %s in all.csv\n",
                                    doiString, title, author);
                            continue;
                        }
                        csv.printRecord(record.get("id"), title, abstractString);
                    }
                }
            }
        }
    }

    private static void updateAll(List<CSVRecord> all, File dir) throws ParseException, IOException {
        File[] files = dir.listFiles(name -> name.getName().endsWith(".bib"));
        if (files == null) throw new IllegalArgumentException("No .bib files");

        BibTeXParser parser = new BibTeXParser();
        try (FileWriter csvFileWriter = new FileWriter(new File(dir, "all.csv.new"));
             CSVPrinter csv = new CSVPrinter(csvFileWriter, CSVFormat.DEFAULT)) {
            csv.printRecord("id", "batch", "bibKey", "doi", "year",
                    "title", "author", "venue");
            //add all previous records
            for (CSVRecord record : all) csv.printRecord(record);

            String idFmt = "043-%03d";
            int idCounter = all.size()+1;
            for (File bibFile : files) {
                try (FileReader bibFileReader = new FileReader(bibFile)) {
                    BibTeXDatabase db = parser.parseFully(bibFileReader);
                    for (Map.Entry<Key, BibTeXEntry> mEntry : db.getEntries().entrySet()) {
                        BibTeXEntry e = mEntry.getValue();
                        Value venue = e.getField(BibTeXEntry.KEY_BOOKTITLE);
                        if (venue == null)
                            venue = e.getField(BibTeXEntry.KEY_JOURNAL);
                        String venueString = venue == null ? null : venue.toUserString();
                        Value doi = e.getField(BibTeXEntry.KEY_DOI);
                        String doiString = doi == null ? null : doi.toUserString();
                        String title = e.getField(BibTeXEntry.KEY_TITLE).toUserString();
                        String author = e.getField(BibTeXEntry.KEY_AUTHOR).toUserString();

                        //avoid creating duplicates
                        if (find(all, doiString, title, author) != null) continue;

                        csv.printRecord(String.format(idFmt, idCounter++),
                                bibFile.getName(), mEntry.getKey(), doiString,
                                e.getField(BibTeXEntry.KEY_YEAR).toUserString(),
                                title, author, venueString);
                    }
                }
            }
        }

        Files.move(new File(dir, "all.csv.new"), new File(dir, "all.csv"));
    }

    private static CSVRecord find(List<CSVRecord> all, String doiString, String title, String author)
            throws IOException {
        for (CSVRecord record : all) {
            if (Objects.equals(record.get("doi"), doiString)) return record;
            boolean match = Objects.equals(record.get("title"), title)
                    && Objects.equals(record.get("author"), author);
            if (match) return record;
        }
        return null;
    }
}
